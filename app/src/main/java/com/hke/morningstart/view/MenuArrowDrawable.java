package com.hke.morningstart.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;

import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;

public class MenuArrowDrawable extends DrawerArrowDrawable {
    /**
     * @param context used to get the configuration for the drawable from
     */
    public MenuArrowDrawable(Context context) {
        super(context);
    }

    public void setMenu(long duration) {
        setShape(false, duration);
    }

    public void setArrow(long duration) {
        setShape(true, duration);
    }

    public void setShape(boolean arrow, long duration) {
        if (!((!arrow && getProgress() == 0f) || (arrow && getProgress() == 1f))) {
            float endProgress = arrow ? 1f : 0f;
            if (duration <= 0) {
                setProgress(endProgress);
            } else {
                ObjectAnimator oa = ObjectAnimator.ofFloat(this, "progress", endProgress);
                oa.setDuration(duration);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    oa.setAutoCancel(true);
                }
                oa.start();
            }
        }
    }
}
