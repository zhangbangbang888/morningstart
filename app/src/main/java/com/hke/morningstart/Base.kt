package com.hke.morningstart

import android.app.Activity
import android.content.Context
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hke.morningstart.widget.*

interface OnProgressDialogListener {
    fun startDialog(msg: String?)

    /**
     * 停止浮动加载进度条
     */
    fun stopDialog()
}

interface LoadingListener : OnProgressDialogListener {
    override fun stopDialog() {
        cancelDialogForLoading()
    }

    override fun startDialog(msg: String?) {
        when (this) {
            is Fragment -> activity
            is Activity -> this
            else -> null
        }?.apply {
            showDialogForLoading(this, msg, true)
        }
    }
}

fun loadingTip(context: Context, listener: LoadingTip.LoadingTipListener? = null): LoadingTip {
    var mLoadingTip = LoadingTip(context)
    mLoadingTip.layoutParams =
        ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    mLoadingTip.setLoadingTip(LoadingTip.LoadStatus.loading)

    var tipListener: LoadingTip.LoadingTipListener? = null
    if (listener != null) {
        tipListener = listener
    } else if (context is LoadingTip.LoadingTipListener) {
        tipListener = context
    }
    mLoadingTip.setOnReloadListener(tipListener)
    return mLoadingTip
}