package com.hke.morningstart.okhttp

import okhttp3.ResponseBody
import retrofit2.http.*

interface AccountService {
    @FormUrlEncoded
    @POST("handler/authentication.ashx")
    suspend fun login(
        @Field("username") name: String,
        @Field("password") password: String,
        @Field("command") command: String = "login",
        @Field("save") save: String = "all"
    ): ResponseBody?

    @GET("{articleId}")
    suspend fun article(
        @Path("articleId") articleId: String
    ): ResponseBody?


    @FormUrlEncoded
    @POST("research/fundschool/CA00005026")
    suspend fun fundschool(
        @Field("__VIEWSTATE") state: String?=null,
        @Field("__VIEWSTATEGENERATOR") generator: String?=null,
        @Field("__EVENTTARGET") target: String?=null,
        @Field("__EVENTARGUMENT") page: Int?
    ): ResponseBody?

    @FormUrlEncoded
    @POST("research/mutualfund/CA00001032")
    suspend fun mutualfund(
        @Field("__VIEWSTATE") state: String?=null,
        @Field("__VIEWSTATEGENERATOR") generator: String?=null,
        @Field("__EVENTTARGET") target: String?=null,
        @Field("__EVENTARGUMENT") page: Int?
    ): ResponseBody?
}