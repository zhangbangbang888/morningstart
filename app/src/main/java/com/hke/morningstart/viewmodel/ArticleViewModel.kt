package com.hke.morningstart.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.hke.morningstart.okhttp.AccountRepo
import com.hke.morningstart.okhttp.Paging
import com.hke.morningstart.okhttp.PagingRequest


class ArticleViewModel(
    private val handle: SavedStateHandle,
    args: Bundle?,
    request: PagingRequest<Any>
) : ViewModel() {
    private val repo = AccountRepo.getInstance()
    val fundschool = Paging(handle, request) {
        repo.fundschool(it)
    }

    val mutualfund= Paging(handle, request) {
        repo.mutualfund(it)
    }
}

class ArticleViewModelFactory(
    owner: SavedStateRegistryOwner,
    private val args: Bundle? = null,
    val request: PagingRequest<Any>
) :
    AbstractSavedStateViewModelFactory(owner, args) {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return ArticleViewModel(handle, args, request) as T
    }
}