package com.hke.morningstart.viewmodel

import android.os.Bundle
import androidx.lifecycle.*
import androidx.savedstate.SavedStateRegistryOwner
import com.hke.morningstart.okhttp.AccountRepo
import com.hke.morningstart.okhttp.Subscriber

class LoginViewModelFactory(owner: SavedStateRegistryOwner, private val args: Bundle? = null) :
    AbstractSavedStateViewModelFactory(owner, args) {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T = LoginViewModel(handle, args) as T
}

class LoginViewModel(handle: SavedStateHandle, args: Bundle?) : ViewModel() {
    private val userRepo = AccountRepo.getInstance()
    val login = Subscriber<Pair<String, String>, Any>(handle) { value ->
        userRepo.login(
            value.first,
            value.second
        )
    }
}