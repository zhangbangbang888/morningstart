package com.hke.morningstart.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.hke.morningstart.R
import com.hke.morningstart.entity.Article

class ArticleAdapter(data: MutableList<Article>? = ArrayList()) :
    BaseQuickAdapter<Article, BaseViewHolder>(R.layout.rv_item_content, data), LoadMoreModule {
    override fun convert(holder: BaseViewHolder, item: Article) {
        holder.apply {
            setText(R.id.title, item.title)
            setText(R.id.author, item.author)
            setText(R.id.time, item.time)
        }
    }
}